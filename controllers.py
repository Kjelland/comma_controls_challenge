class BaseController:
  def __init__(self):
    self.state = 0
    self.mem = 0
    self.last = 0
    self.current_output = 0

  def update(self, target_lataccel, current_lataccel, state):
    raise NotImplementedError



class OpenController(BaseController):
  def update(self, target_lataccel, current_lataccel, state):
    return target_lataccel

class SimpleController(BaseController):
  def update(self, target_lataccel, current_lataccel, state):
    return (target_lataccel - current_lataccel) * 0.3

class NewController(BaseController):
  def update(self, target_lataccel, current_lataccel, state):

    if abs(target_lataccel - current_lataccel) < 0.01:
      return self.current_output
    self.mem = self.mem * 0.95 + state.roll_lataccel * 0.05
    roll_lataccel = (state.roll_lataccel - self.mem) * -2

    control = (target_lataccel - current_lataccel) * 10

    gamma = 0.99
    self.current_output = self.current_output * gamma + (control + roll_lataccel) * (1.0 - gamma)


    return self.current_output

CONTROLLERS = {
  'open': OpenController,
  'simple': SimpleController,
  'new': NewController,
}
